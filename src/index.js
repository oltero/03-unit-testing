
const mylib = require('./mylib');

const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/addi', (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  res.send(mylib.addi(a,b).toString());
});

app.get('/multi', (req, res) => {
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);
  res.send(mylib.multi(a,b).toString());
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
