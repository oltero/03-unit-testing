const expect = require('chai').expect;
const assert = require('chai').assert;
const mylib = require('../src/mylib');


describe('Unit testing mylib.js', () => {

  before(() => console.log("Beginning testing for mylib"));
  after(() => console.log("Finished with testing"));


  it('Should return 2 when using addi function with a=1 and b=1', () => {
    const result = mylib.addi(1,1);
    expect(result).to.equal(2);
  })

  it('Non-negative values are also allowed: -1 + -5 = -6', () => {
    const result = mylib.addi(-1,-5);
    expect(result).to.equal(-6);
  })

  it('Any two non-zero elements sum to different number', () => {
    const result = mylib.addi(1, 1);
    assert(1 !== mylib.addi(1,1) !== 1 && 1 !== 0);
  })

  it('Should return 10 when using multi function with a=2 and b=5', () => {
    const result = mylib.multi(2,5);
    expect(result).to.equal(10);
  })
});